@extends('layouts.simple')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Email Verification Successful</div>

                    <div class="card-body">
                        <p>Your email has been successfully verified.</p>
                        <p>Please <a href="{{ config('app.web_url') }}">login</a> to access your account.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection