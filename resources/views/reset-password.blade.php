@extends('layouts.simple')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" style="display: none;"></div>
                <div class="alert alert-danger" style="display: none;"></div>
                <div class="card">
                    <div class="card-header">Reset Password</div>

                    <div class="card-body">
                        <form id="resetForm" method="POST">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group row mb-2">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                                    <span class="form-error" id="password-error"></span>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="confirm_password" required autocomplete="new-password">
                                    <span class="form-error" id="confirm_password-error"></span>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="button" id="resetButton" class="btn btn-primary">
                                        {{ __('Reset Password') }}
                                    </button>
                                </div>
                            </div>                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        $("#resetButton").on("click", function () {
            var formData = $("#resetForm").serialize();

            // Clear previous errors
            $(".form-error").text("");

            $.ajax({
                type: "POST",
                url: "{{ route('password.update') }}",
                data: formData,
                success: function (data) {
                    $("#resetForm").trigger("reset");
                    $(".alert-success").text(data.message).fadeIn().delay(3000).fadeOut();
                    setTimeout(function () {
                        window.location.href = "{{ config('app.web_url') }}";
                    }, 4000);
                },
                error: function (error) {
                    if (error.status === 422) {
                        // Handle validation errors
                        var errors = error.responseJSON.data;

                        errors.forEach((error) => {
                            if (error.key == 'token') {
                                $(".alert-danger").text(error.value).fadeIn().delay(3000).fadeOut();
                            } else {
                                $("#" + error.key + "-error").text(error.value);
                            }
                        });
                    } else if (error.status === 409) {
                        // Handle validation errors
                        var errors = error.responseJSON.data;

                        $(".alert-danger").text(errors.join("\r\n")).fadeIn().delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").text(error.responseJSON.message).fadeIn().delay(3000).fadeOut();
                    }
                }
            });
        });
    });
</script>
@endsection