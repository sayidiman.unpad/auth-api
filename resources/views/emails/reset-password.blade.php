<x-mail::message>
# We have received your request to reset your account password

You can use the following link to recover your account:

<x-mail::button :url="$url">
Reset Password
</x-mail::button>

The allowed duration of the link is one hour from the time the message was sent.

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
