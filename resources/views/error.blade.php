@extends('layouts.simple')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Internal Server Error</div>

                    <div class="card-body">
                        <p>Sorry, something went wrong on our end.</p>
                        <p>We are working to fix the issue. Please try again later.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection