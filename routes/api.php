<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return 'API V1';
});

Route::group([
    'prefix' => 'v1',
], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::get('email/verify/{id}', [AuthController::class, 'verify'])->name('verification.verify');
    Route::get('email/resend/{email}', [AuthController::class, 'resendEmail'])->name('verification.resend.email');
    Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('reset-password', [AuthController::class, 'resetPassword'])->name('password.update');


    Route::middleware('auth:api')->group( function () {
        Route::get('email/resend', [AuthController::class, 'resend'])->name('verification.resend');
    });
});

Route::get('/verified', function () {
    return view('verified');
})->name('verified');
Route::get('/reset-password/{token}', function (string $token) {
    return view('reset-password', ['token' => $token]);
})->name('password.reset');