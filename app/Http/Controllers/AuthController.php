<?php

namespace App\Http\Controllers;

use App\Http\Response;
use App\Mail\SendResetPassword;
use App\Models\PasswordResetToken;
use App\Models\User;
use Exception;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public const INTERNAL_SERVER_ERROR = "Internal server error.";
    public const REGISTER_SUCCESS = 'Data is successfully registered.';
    public const LOGIN_SUCCESS = 'User login successfully.';
    public const SENT_VERIFICATION_LINK = 'Email verification link sent on your email.';
    public const VERIFY_SUCCESS = 'Email successfully verified. Please login in login page.';
    public const NEED_VERIFIED = 'Please verify email.';
    public const ALREADY_VERIFIED = 'Email already verified.';
    public const INVALID_URL = 'Invalid/expired url provided.';
    public const SENT_RESET_LINK = 'Reset password link sent on your email.';
    public const RESET_SUCCESS = 'Reset password successfully. Please login in login page.';
    public const TOKEN_EXPIRED = 'Token already expired.';

    /**
     * Register new user.
     */
    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'name' => ['required', 'string'],
                'username' => ['required', 'string', 'unique:users,username'],
                'email' => ['required', 'email', 'unique:users,email'],
                'password' => ['required'],
                'confirm_password' => ['required', 'same:password'],
            ]);

            $user = new User();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $user->sendEmailVerificationNotification();

            DB::commit();

            $data = [
                'token' => $user->createToken(config('app.name'))->accessToken
            ];

            return Response::success($data, message: self::SENT_VERIFICATION_LINK);
        } catch (ValidationException $ve) {
            DB::rollBack();

            return Response::validation($ve);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }

    /**
     * Login user.
     */
    public function login(Request $request)
    {
        try {
            if (
                !Auth::attempt(['username' => $request->username, 'password' => $request->password]) &&
                !Auth::attempt(['email' => $request->username, 'password' => $request->password])
            ) {
                return Response::unauthenticated();
            }

            $user = User::find(Auth::user()->id);

            if (!$user->hasVerifiedEmail()) {
                return Response::fail(self::NEED_VERIFIED, 401);
            }

            $data = [
                'token' => $user->createToken(config('app.name'))->accessToken
            ];

            return Response::success($data, message: self::LOGIN_SUCCESS);
        } catch (Exception $e) {
            report($e);

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }

    /**
     * Verification email.
     */
    public function verify(Request $request, $userId)
    {
        try {
            if (!$request->hasValidSignature()) {
                return Response::fail(self::INVALID_URL, 401);
            }

            $user = User::findOrFail($userId);
            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();
            }

            return view('verified');
        } catch (Exception $e) {
            report($e);

            return view('error');
        }
    }

    /**
     * Resend verification email.
     */
    public function resend()
    {
        try {
            $user = User::find(Auth::user()->id);
            if ($user->hasVerifiedEmail()) {
                return Response::fail(self::ALREADY_VERIFIED, 400);
            }

            $user->sendEmailVerificationNotification();

            return Response::success([], message: self::SENT_VERIFICATION_LINK);
        } catch (Exception $e) {
            report($e);

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }

    /**
     * Resend verification email.
     */
    public function resendEmail(string $email)
    {
        try {
            $user = User::query()
                ->where('email', $email)
                ->first();

            if (!$user) {
                return Response::notFound();
            }

            if ($user->hasVerifiedEmail()) {
                return Response::fail(self::ALREADY_VERIFIED, 400);
            }

            $user->sendEmailVerificationNotification();

            return Response::success([], message: self::SENT_VERIFICATION_LINK);
        } catch (Exception $e) {
            report($e);

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }

    /**
     * Forgot password
     */
    public function forgotPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'email' => ['required', 'email', 'exists:users'],
            ]);

            // Delete all old code that user send before.
            PasswordResetToken::where('email', $request->email)->delete();

            // Generate random code
            $token = uniqid();

            // Register new code
            $passwordReset = new PasswordResetToken();
            $passwordReset->email = $request->email;
            $passwordReset->token = $token;
            $passwordReset->created_at = now();
            $passwordReset->save();

            DB::commit();

            // Send email to user
            Mail::to($request->email)->send(new SendResetPassword($passwordReset->token));

            return Response::success([], message: self::SENT_RESET_LINK);
        } catch (ValidationException $ve) {
            DB::rollBack();

            return Response::validation($ve);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }

    /**
     * Forgot password
     */
    public function resetPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'token' => ['required', 'string', 'exists:password_reset_tokens'],
                'password' => ['required'],
                'confirm_password' => ['required', 'same:password'],
            ]);

            // find the code
            $passwordReset = PasswordResetToken::query()
                ->where('token', $request->token)
                ->first();

            if (!$passwordReset) {                
                return Response::notFound();
            }

            $messages = [];
            if (now() > $passwordReset->created_at->addHour()) {
                $passwordReset->delete();
                $message = self::TOKEN_EXPIRED;
                $messages[] = $message;
            }

            if (!empty($messages)) {
                return Response::manualValidation($messages);
            }

            // find user's email 
            $user = User::query()
                ->where('email', $passwordReset->email)
                ->first();

            $user->password = bcrypt($request->password);
            $user->save();

            // delete current code 
            $passwordReset->delete();

            DB::commit();

            return Response::success([], message: self::RESET_SUCCESS);
        } catch (ValidationException $ve) {
            DB::rollBack();

            return Response::validation($ve);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();

            return Response::fail(self::INTERNAL_SERVER_ERROR, 503);
        }
    }
}
