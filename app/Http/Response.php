<?php

namespace App\Http;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class Response
{
    const MSG_SUCCESS = 'Success.';
    const MSG_UNAUTHENTICATED = 'Unauthenticated.';
    const MSG_FORBIDDEN = 'You cannot access this resource.';
    const MSG_FAIL = 'Failed.';
    const MSG_VALIDATION_ERROR = 'Validation error.';
    const MSG_NOT_FOUND = 'Not Found.';
    const MSG_DATABASE_ERROR = 'Database Error.';
    const MSG_AUTH_ERROR = 'Auth Error.';

    public static function success(
        $data = [],
        $message = null,
        $metadata = null,
        $statusCode = null
    ): \Illuminate\Http\Response {
        $response = [
            'message' => $message ?: self::MSG_SUCCESS,
            'data' => $data,
        ];

        if (! empty($metadata)) {
            $response['metadata'] = $metadata;
        }

        if (! empty($data)) {
            $dataCount = sizeof($data);
            if (! isset($response['metadata']['result_count'])) {
                $response['metadata']['result_count'] = $dataCount;
            }
        }

        return response($response, $statusCode ?: 200);
    }

    public static function fail($message = null, $statusCode = null): \Illuminate\Http\Response
    {
        return response(
            [
                'message' => $message ?: self::MSG_FAIL,
            ],
            $statusCode ?: 503
        );
    }

    public static function failWithData($data = null, $message = null, $statusCode = null): \Illuminate\Http\Response
    {
        $response = [
            'message' => $message ?: self::MSG_FAIL,
            'data' => $data ?? [],
        ];

        if (! empty($data)) {
            $dataCount = sizeof($data);
            if (! isset($response['metadata']['result_count'])) {
                $response['metadata']['result_count'] = $dataCount;
            }
        }

        return response($response, $statusCode ?: 200);
    }

    public static function unauthenticated(): \Illuminate\Http\Response
    {
        return response(['message' => self::MSG_UNAUTHENTICATED], 401);
    }

    public static function forbidden(): \Illuminate\Http\Response
    {
        return response(['message' => self::MSG_FORBIDDEN], 403);
    }

    public static function validation(ValidationException $ve): \Illuminate\Http\Response
    {
        $messages = [];
        foreach ($ve->errors() as $key => $error) {
            $message = [
                'key' => $key,
                'value' => $error[0],
            ];
            $messages[] = $message;
        }

        return response([
            'message' => self::MSG_VALIDATION_ERROR,
            'data' => $messages,
        ], 422);
    }

    public static function manualValidation($messages): \Illuminate\Http\Response
    {
        if (! is_array($messages)) {
            $messages = [$messages];
        }

        return response([
            'message' => self::MSG_VALIDATION_ERROR,
            'data' => $messages,
        ], 409);
    }

    public static function notFound($message = null): \Illuminate\Http\Response
    {
        return response([
            'message' => $message ?: self::MSG_NOT_FOUND,
        ], 404);
    }
}
